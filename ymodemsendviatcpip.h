#ifndef YMODEMSENDVIATCPIP_H
#define YMODEMSENDVIATCPIP_H

#include <QObject>
#include <QTcpSocket>
#include <QDebug>
#include <QFileInfo>
#include <QHostAddress>
#include <QtConcurrent/QtConcurrentRun>
#include "ymodemprotocol.h"

class YmodemSendViaTCPIP : public QObject
{
    Q_OBJECT
    QHostAddress mip;
    quint16 mport;
    QString mfileName;
    QTcpSocket *tcpsocket;
    const quint32 waitingTime = 10 * 1000;//in seconds
    const quint32 maxAttemp = 10;

    bool connected = false;
    bool m_cancel = false;

    YmodemProtocol *ymodemProtocol;



    Q_PROPERTY(QString ip READ ip WRITE setIp )
    Q_PROPERTY(QString fileName READ fileName WRITE setFileName )
    Q_PROPERTY(quint16 port READ port WRITE setPort )
    void startSendThread();
    char readProcess();
    void writeProcess(const QByteArray &ba);
    void writeProcess(const int c);

public:
    explicit YmodemSendViaTCPIP(QObject *parent = 0);
    QString ip();
    QString fileName();
    quint16 port();

signals:
    void sendComplete();
    void sendStatus(QString status);
    void progress(double);
public slots:
    void setIp(QString ip);
    void setPort(quint16 port);
    void setFileName(QString fileName);
    void startSend();

private:
    YMODEMResponce transmitClose();
    YMODEMResponce transmitPacket(const QFileInfo &fileInfo, QFile &dev, quint8 sequenceNum = 0, bool firstPacket = true);
};

#endif // YMODEMSENDVIATCPIP_H
