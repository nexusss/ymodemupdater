#ifndef YMODEMPROTOCOL_H
#define YMODEMPROTOCOL_H

#include <QObject>

#define SOH  0x01
#define STX  0x02
#define EOT  0x04
#define ACK  0x06
#define NAK  0x15
#define CAN  0x18
#define CTRLZ 0x1A

//enum class MESSAGE{SOH = 0x01,STX = 0x02,EOT = 0x04,ACK = 0x06,NAK = 0x15,CAN = 0x18,CTRLZ = 0x1a};

enum class YMODEMResponce {
        MODEM_SUCCESS = 0,
        MODEM_PACKET_FAILED = -1,
        MODEM_NO_SYNC = -2,
        MODEM_REMOTE_CANCEL = -3,
        MODEM_EOT_ERROR = -4,
        MODEM_LOCAL_CANCEL = -5,
        MODEM_SIZE_ERROR = -6,
        MODEM_RESTART = -7,
        MODEM_FILE_NOT_FOUND_ERROR = -8,
        MODEM_FILE_NOT_OPEN_ERROR = -9
};

class YmodemProtocol : public QObject
{
    Q_OBJECT

public:
    explicit YmodemProtocol(QObject *parent = 0);
    const static quint32 payloadSize = 1024;


    unsigned short crc16_ccitt( const unsigned char *buf, int len );
    QByteArray createPacket(int sequence);
signals:

public slots:
};

#endif // YMODEMPROTOCOL_H
