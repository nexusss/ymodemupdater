#include "ymodemsendviatcpip.h"
#include <QDataStream>
#include <QDateTime>

YmodemSendViaTCPIP::YmodemSendViaTCPIP(QObject *parent) : QObject(parent)
{
    tcpsocket = new QTcpSocket(this);
    ymodemProtocol = new YmodemProtocol(this);
    typedef void (QAbstractSocket::*QAbstractSocketErrorSignal)(QAbstractSocket::SocketError);
         qRegisterMetaType<QAbstractSocket::SocketError>("QAbstractSocket::SocketError");
        connect(tcpsocket, static_cast<QAbstractSocketErrorSignal>(&QAbstractSocket::error),
                this, [=] () {qDebug()<< "error" << tcpsocket->errorString();emit sendStatus(tcpsocket->errorString());});
}

void YmodemSendViaTCPIP::setIp(QString ip){
    mip.setAddress(ip);
}

void YmodemSendViaTCPIP::setPort(quint16 port){
    if(mport != port)
        mport = port;
}

void YmodemSendViaTCPIP::setFileName(QString fileName){
    if(this->mfileName != fileName)
        this->mfileName = fileName ;
}

QString YmodemSendViaTCPIP::ip(){
    return mip.toString();
}

QString YmodemSendViaTCPIP::fileName(){
    return mfileName;
}

quint16 YmodemSendViaTCPIP::port(){
    return mport;
}

char YmodemSendViaTCPIP::readProcess(){
    tcpsocket->waitForReadyRead(waitingTime);
    if(tcpsocket->bytesAvailable()){
        char message;
        tcpsocket->read(&message,1);
        return message;
    }

    return 0xff;
}

void YmodemSendViaTCPIP::writeProcess(const QByteArray &ba){
    if(connected){
        tcpsocket->write(ba);
        tcpsocket->waitForBytesWritten(1000);
    }
}

void YmodemSendViaTCPIP::writeProcess(const int c){
    if(connected){
        QByteArray ba;
        ba.append(QChar(c));
        tcpsocket->write(ba);
        tcpsocket->waitForBytesWritten(1000);
    }
}

YMODEMResponce YmodemSendViaTCPIP::transmitPacket(const QFileInfo &fileInfo,QFile &dev, quint8 sequenceNum,bool firstPacket){
    QByteArray payload;
        char res;
        if (dev.atEnd())
        {
            for (int retry = 0; retry < maxAttemp; ++retry) {
                writeProcess(EOT);
                res = readProcess();
                if (res == ACK) break;
            }
            return (res == ACK)?YMODEMResponce::MODEM_SUCCESS: YMODEMResponce::MODEM_EOT_ERROR;
        }
        if (firstPacket)
        {
            payload += fileInfo.fileName().toUtf8();
            payload += (quint32) fileInfo.size();
            payload += (quint32) fileInfo.created().toTime_t();
        }
        else
        {
            payload = dev.read(YmodemProtocol::payloadSize);
        }
        payload += QString(CTRLZ).repeated(YmodemProtocol::payloadSize - payload.size()).toUtf8();
        QByteArray packet;
        packet.append(QString(STX).toUtf8());
        packet.append(sequenceNum);
        packet.append(~sequenceNum);
        packet.append(payload);
        unsigned short ccrc = ymodemProtocol->crc16_ccitt((unsigned char*)payload.constData(),YmodemProtocol::payloadSize);
        packet.append((unsigned short)(ccrc>>8) & 0xFF);
        packet.append((unsigned short)ccrc & 0xFF);
        int pos = dev.pos();
        qDebug() << "XMODEM:xBeginTransmit - Packet # : " << pos/YmodemProtocol::payloadSize << "/" << (int)floor((float)fileInfo.size()/YmodemProtocol::payloadSize + 0.5f);
        //emit message(QString("Sending Packet %1/%2").arg(pos/YmodemProtocol::payloadSize).arg((int)floor((float)fileInfo.size()/YmodemProtocol::payloadSize)));
        emit sendStatus(QString("Sending Packets ..."));
        emit progress(floor((float)pos/fileInfo.size()*100.0f));
        Q_ASSERT(packet.size() == 1029);
        for (int retry = 0; retry < maxAttemp && !m_cancel; ++retry) {
            writeProcess(packet);
            res = readProcess();
            if (res >= 0 ) {
                switch (res) {
                case ACK:
                    qDebug() << "ACK";
                    return transmitPacket(fileInfo,dev,sequenceNum+1,false);
                case CAN:
                    qDebug() << "CAN";
                    res = readProcess();
                    if (res == CAN) {
                        writeProcess(ACK);
//                        yTransmitFlush();
                        qDebug() << "XMODEM::xBeginTransmit - canceled by remote";
                        return YMODEMResponce::MODEM_REMOTE_CANCEL;
                    }
                    break;
                case NAK:
                    qDebug() << "NAK";
                    break;
                case 'c':
                case 'C':
                    qDebug() << "C";
                    retry--;
                    continue;
                case '>':
                    return YMODEMResponce::MODEM_RESTART;
                case '\0':
                    qDebug() << "\\0";
                    break;
                default:
                    qDebug() << "XYMODEM::yTransmitPacket - Unknown " << res;
                    break;
                }
            }
        }
        writeProcess(CAN);
        writeProcess(CAN);
        writeProcess(CAN);
//        yTransmitFlush();
        if (m_cancel)
            return YMODEMResponce::MODEM_LOCAL_CANCEL;
        else
    return YMODEMResponce::MODEM_PACKET_FAILED;
}

void YmodemSendViaTCPIP::startSendThread(){
    tcpsocket->connectToHost(mip,mport);
    connected = false;
    if(!tcpsocket->waitForConnected(5000)){
        qDebug() << "not connected to host";
        emit sendStatus("not connected to host");

        return;
    }

    qDebug() << "start send thread";
    connected = true;

    QFile file(mfileName);
    if(!file.open(QIODevice::ReadOnly)){
        qDebug() << "can't open file";
        emit sendStatus("can't open file");
    }

    QFileInfo fileInfo(mfileName);



    for(int retry = 0; retry < maxAttemp && !m_cancel; ++retry)
    {
    char message = readProcess();
    switch(message){
        case '\0':
        break;
        case 'c':
            qDebug() << "read c";
        break;
        case 'C':
            qDebug() << "read C";
        break;
    case NAK:{
        qDebug() << "NAK";
        YMODEMResponce response;
        response = transmitPacket(fileInfo,file);
        if (response == YMODEMResponce::MODEM_RESTART){
            file.seek(0);
            retry = 0;
            continue;
        }
//        return response;
    break;
    }
    case CAN:
        qDebug() << "XMODEM::yTransmit - canceled by remote";
        emit sendStatus("Transmit - canceled by remote");
        /*message = readProcess();
        if (message == CAN) {
            writeProcess(ACK);
            qDebug() << "XMODEM::yTransmit - canceled by remote";
            emit
//            return YMODEMResponce::MODEM_REMOTE_CANCEL;
        }*/
    break;
    default:
        qDebug() << "unlnown message " << message;
        emit sendStatus("unlnown message " + QString(message));
    }
    }
    qDebug() << "file close";

    file.close();
}

YMODEMResponce YmodemSendViaTCPIP::transmitClose(){
    QByteArray payload;
        payload += QString().toUtf8();
        int zero = 0;
        payload += (quint32) zero;
        payload += (quint32) zero;
        payload += QString(CTRLZ).repeated(YmodemProtocol::payloadSize - payload.size()).toUtf8();
        QByteArray packet;
        packet.append(QString(STX).toUtf8());
        int sequenceNum = 0;
        packet.append(sequenceNum);
        packet.append(~sequenceNum);
        packet.append(payload);
        unsigned short ccrc = ymodemProtocol->crc16_ccitt((unsigned char*)payload.constData(),1024);
        packet.append((unsigned short)(ccrc>>8) & 0xFF);
        packet.append((unsigned short)ccrc & 0xFF);
        int res;
        for (int retry = 0; retry < 10; ++retry)
        {
            writeProcess(packet);
            res = readProcess();
            if ( res == ACK) break;
        }
    return (res == ACK)?YMODEMResponce::MODEM_SUCCESS: YMODEMResponce::MODEM_EOT_ERROR;
}

void YmodemSendViaTCPIP::startSend(){
    QtConcurrent::run(this,&YmodemSendViaTCPIP::startSendThread);
}
