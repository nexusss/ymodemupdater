import QtQuick 2.7
import QtQuick.Window 2.0
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.0
import QtQuick.Dialogs 1.2
import YModem.tcpip 1.0

ApplicationWindow {
    id: applicationWindow1
    visible: true
    width: 640
    height: 480
    title: qsTr("ymodem updater")

    YmodemTCPIP{
        id: ymodemTcpip
        port:  parseInt(portInput.text)
    }

    FileDialog{
        id: fd
        onAccepted: {
            fileNameText.text = fileUrl
            ymodemTcpip.setFileName(fileUrl)
        }
    }

    Button {
        id: updatebutton
        x: 179
        y: 125
        text: "Start send"
    }

    Button {
        id: chooseFileButton
        x: 10
        y: 125
        text: qsTr("Choose file")
    }

    TextInput {
        id: iptInput
        x: 10
        y: 39
        width: 183
        height: 35
        text: ""
        inputMask: "000.000.000.000"
        font.pixelSize: 24
    }

    Text {
        id: fileNameText
        x: 10
        y: 80
        width: 269
        height: 32
        font.pixelSize: 12
    }

    TextInput {
        id: portInput
        x: 199
        y: 39
        width: 80
        height: 35
        text: qsTr("")
        horizontalAlignment: Text.AlignHCenter
        inputMask: "00000"
        font.pixelSize: 24
    }

    Text {
        id: text2
        x: 10
        y: 9
        width: 183
        height: 29
        text: qsTr("Ip")
        horizontalAlignment: Text.AlignHCenter
        font.pixelSize: 24
    }

    Text {
        id: text3
        x: 199
        y: 9
        width: 80
        height: 29
        text: qsTr("Port")
        horizontalAlignment: Text.AlignHCenter
        font.pixelSize: 24
    }

    Text {
        id: text1
        x: 10
        y: 171
        width: 85
        height: 33
        text: qsTr("Status :")
        font.pixelSize: 24
    }

    Text {
        id: statusText
        x: 101
        y: 171
        width: 178
        height: 33
        text: qsTr("")
        font.pixelSize: 24
    }


}
